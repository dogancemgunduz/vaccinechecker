package com.dcgunduz.prullenbakvaccin.prullenbak;

import lombok.SneakyThrows;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PrullenbakClient {

    private final String SITE_NAME = "https://www.prullenbakvaccin.nl/";
    private final String PRIVATE_TOKEN = "kZc7xb8DMyVRPJy1AqiHAKqPyQZW1oY5fEjnO3NQ";

    private static final Logger logger = LoggerFactory.getLogger(PrullenbakClient.class);

    @SneakyThrows
    public boolean vaccinesAvailable(String location) {

        Map<String, String> headers = Map.of(
                "accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "content-type", "application/x-www-form-urlencoded",
                "cache-control", "max-age=0",
                "cookie", "prullenbakvaccin_session=eyJpdiI6IllLb1M2dGQzblJjdVAwckwrNlp0akE9PSIsInZhbHVlIjoiYVhaSWhKRjQ1eXZBbVVkeUNRbVE3aU5zNkhYQkJoa1g4OC9zYVZ0S2F0ZjBTL0JzK3ZEWDkwcytudVVjWktaZzlTWUVsdFMzQnd3N21mQjRQa0RYVCs5a2dEM0tMekd3WE9aYXc0MVFHamlEczhkdjlUYUtxWm1QZGJOb1JyL20iLCJtYWMiOiI0NTIyYzFjM2I3NWU1ZGYwZTZmOGM5MjNlYTRmZDYxMzc0NmY5MjQ5NWQ5M2YwYjE5YjBlMzljODRmYWU4ZTMwIn0="
        );

        Connection.Response response = Jsoup.connect(SITE_NAME)
                .headers(headers)
                .data(
                        "location", location
                )
                .data(
                        "_token", PRIVATE_TOKEN
                )
                .referrer(SITE_NAME)
                .followRedirects(true)
                .ignoreHttpErrors(true)
                .ignoreContentType(true)
                .userAgent("Mozilla/5.0 AppleWebKit/537.36 (KHTML," +
                        " like Gecko) Chrome/45.0.2454.4 Safari/537.36")
                .method(Connection.Method.POST)
                .timeout(0)
                .execute();

        int indexOfNumber = response.body().indexOf("locaties binnen 20km van") - 3;
        String clinicCount = response.body().substring(indexOfNumber, indexOfNumber+2);

        int totalPlacesWithoutAVaccine = 0;

        String responseBody = response.body();
        String searchable = "geen vaccins";
        while(responseBody.contains(searchable)) {
            responseBody = responseBody.substring(responseBody.indexOf(searchable) + searchable.length());
            totalPlacesWithoutAVaccine++;
        }

        logger.info("Prullenbak check finalized, result returned");
        return Integer.valueOf(clinicCount.trim()) != totalPlacesWithoutAVaccine- 1;
    }
}
