package com.dcgunduz.prullenbakvaccin.scheduler;

import com.dcgunduz.sound.SoundPlayer;
import com.dcgunduz.prullenbakvaccin.prullenbak.PrullenbakClient;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.stream.IntStream;

@Component
public class VaccineChecker {

    private final String ALERT_LOCATION = "/champions.wav";

    private final PrullenbakClient prullenbakClient;

    private static final Logger logger = LoggerFactory.getLogger(VaccineChecker.class);

    @Value("${locations}")
    private String commaSeperatedLocations;

    public VaccineChecker(PrullenbakClient prullenbakClient) {
        this.prullenbakClient = prullenbakClient;
    }

    @Scheduled(cron = "0 0/5 * * * *")
    public void pollAvailableVaccines() {

        logger.info("Scheduler kicked in");
        for(String location : commaSeperatedLocations.split(",")) {
            if(prullenbakClient.vaccinesAvailable(location)) {
                logger.info("Vaccine found : " + location);
                IntStream.range(0,10).forEach(
                        range -> alert()
                );
            } else {
                logger.info("No Vaccines found : " + location);
            }
        }
    }

    @SneakyThrows
    private void alert() {

        SoundPlayer soundPlayer = new SoundPlayer();
        soundPlayer.playSound(getClass().getResource(ALERT_LOCATION).getPath());
    }
}
