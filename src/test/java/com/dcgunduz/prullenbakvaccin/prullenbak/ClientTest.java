package com.dcgunduz.prullenbakvaccin.prullenbak;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClientTest {

    PrullenbakClient prullenbakClient = new PrullenbakClient();

    @Test
    public void clientConnects() {
        Assertions.assertDoesNotThrow(
                () -> {
                    prullenbakClient.vaccinesAvailable("Amsterdam");
                }
        );
    }

    @Test
    public void vaccinesNotAvailable() {
        Assertions.assertEquals(
                prullenbakClient.vaccinesAvailable("Amsterdam"), false
        );

        Assertions.assertEquals(
                prullenbakClient.vaccinesAvailable("Utrecht"), false
        );

        Assertions.assertEquals(
                prullenbakClient.vaccinesAvailable("Rotterdam"), false
        );
    }
}
