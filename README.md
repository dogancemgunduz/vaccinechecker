A utility tool I made for myself to check for vaccines near me. Standard spring boot project, running would trigger the 5 min schedule to check for nearby locations. Will play an alert if there is a match =)
 
Feel free to alter the locations under application.yml or fork and use this any way you like.

Cheers !
